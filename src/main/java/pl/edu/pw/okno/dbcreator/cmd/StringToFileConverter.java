package pl.edu.pw.okno.dbcreator.cmd;

import com.beust.jcommander.IStringConverter;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.io.File;

public class StringToFileConverter implements IStringConverter<File> {

  @Override
  public File convert(String value) {
    Preconditions.checkArgument(!Strings.isNullOrEmpty(value), "file path is null or empty");
    return new File(value);
  }
}
