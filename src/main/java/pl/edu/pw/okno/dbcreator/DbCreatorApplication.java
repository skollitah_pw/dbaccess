package pl.edu.pw.okno.dbcreator;

import com.beust.jcommander.JCommander;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.edu.pw.okno.dbcreator.cmd.ExecSqlCommand;
import pl.edu.pw.okno.dbcreator.handler.ExecSqlProcessor;
import pl.edu.pw.okno.dbcreator.sql.SqlExecutor;

import javax.sql.DataSource;

public class DbCreatorApplication {

  public static void main(String[] args) {
    ExecSqlCommand execSqlCommand = new ExecSqlCommand();
    JCommander jc = JCommander.newBuilder().addCommand(execSqlCommand).build();
    jc.parse(args);

    if (ExecSqlCommand.NAME.equals(jc.getParsedCommand())) {
      PropertiesReader propertiesReader = new PropertiesReader();
      DataSource dataSource =
          new DataSourceFactory(propertiesReader).createDataSource(execSqlCommand.getConfigFile());
      SqlExecutor sqlExecutor = new SqlExecutor(new JdbcTemplate(dataSource));
      new ExecSqlProcessor(execSqlCommand, sqlExecutor).process();
    } else {
      throw new IllegalArgumentException("Unknown command");
    }
  }
}
