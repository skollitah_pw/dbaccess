package pl.edu.pw.okno.dbcreator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

  public Properties read(File propertiesFile) {
    Properties prop = new Properties();

    try (FileInputStream fis = new FileInputStream(propertiesFile)) {
      prop.load(fis);
      return prop;
    } catch (IOException e) {
      throw new RuntimeException("Error reading config file");
    }
  }
}
