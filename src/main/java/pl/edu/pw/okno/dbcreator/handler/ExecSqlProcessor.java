package pl.edu.pw.okno.dbcreator.handler;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import pl.edu.pw.okno.dbcreator.cmd.ExecSqlCommand;
import pl.edu.pw.okno.dbcreator.sql.SqlExecutor;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

@RequiredArgsConstructor
public class ExecSqlProcessor {

  private final ExecSqlCommand execSqlCommand;
  private final SqlExecutor sqlExecutor;

  public void process() {
    if (bothSqlSrcDefined()) {
      throw new IllegalArgumentException(
          "Both sql command and sql file defined. Please choose only one sql source");
    } else if (noSqlSrcDefined()) {

      throw new IllegalArgumentException(
          "No sql source defined. Please provide sql command or sql file location");
    } else if (sqlFileSrcDefined()) {
      executeFile();
    } else if (sqlCmdSrcDefined()) {
      sqlExecutor.execute(execSqlCommand.getSqlCommand());
    }
  }

  private boolean sqlCmdSrcDefined() {
    return execSqlCommand.getSqlCommand() != null;
  }

  private boolean sqlFileSrcDefined() {
    return execSqlCommand.getSqlFile() != null;
  }

  private boolean bothSqlSrcDefined() {
    return sqlCmdSrcDefined() && sqlFileSrcDefined();
  }

  private boolean noSqlSrcDefined() {
    return !sqlCmdSrcDefined() && !sqlFileSrcDefined();
  }

  private void executeFile() {
    try {
      String sql =
          IOUtils.toString(
              new FileInputStream(execSqlCommand.getSqlFile()), Charset.defaultCharset());
      String[] statements = sql.split(";");
      Arrays.stream(statements).filter(s -> s.trim().length() > 0).forEach(sqlExecutor::execute);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
