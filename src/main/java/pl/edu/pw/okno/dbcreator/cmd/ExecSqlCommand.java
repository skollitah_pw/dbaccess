package pl.edu.pw.okno.dbcreator.cmd;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.File;

import static pl.edu.pw.okno.dbcreator.cmd.ExecSqlCommand.NAME;

@Parameters(commandNames = NAME, commandDescription = "Executes sql command")
@Getter
@Setter
@ToString
public class ExecSqlCommand {

  public static final String NAME = "exec";

  @Parameter(names = "-s", description = "Sql command")
  private String sqlCommand;

  @Parameter(names = "-f", description = "Sql file location")
  private File sqlFile;

  @Parameter(
      names = "-c",
      converter = StringToFileConverter.class,
      description = "Config file location",
      required = true)
  private File configFile;
}
