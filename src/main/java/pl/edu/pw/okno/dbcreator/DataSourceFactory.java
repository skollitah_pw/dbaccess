package pl.edu.pw.okno.dbcreator;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.util.Properties;

@RequiredArgsConstructor
public class DataSourceFactory {

  private final PropertiesReader propertiesReader;

  public DataSource createDataSource(File propertiesFile) {
    Preconditions.checkArgument(propertiesFile != null, "propertiesFile is null");

    Properties properties = propertiesReader.read(propertiesFile);
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    //dataSource.setDriverClassName("net.ucanaccess.jdbc.UcanaccessDriver");
    dataSource.setUrl(getUrl(properties));
    dataSource.setUsername("");
    dataSource.setPassword("");

    return dataSource;
  }

  private String getUrl(Properties properties) {
    String url = properties.getProperty("db.url");

    if (Strings.isNullOrEmpty(url)) {
      throw new IllegalArgumentException(
          "Database connection string is not provided in config file");
    }

    return url;
  }
}
