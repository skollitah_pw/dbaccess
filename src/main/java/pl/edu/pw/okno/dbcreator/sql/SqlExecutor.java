package pl.edu.pw.okno.dbcreator.sql;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;

@RequiredArgsConstructor
public class SqlExecutor {

  private final JdbcTemplate jdbcTemplate;

  public void execute(String sqlCommand) {
    jdbcTemplate.execute(sqlCommand);
  }
}
