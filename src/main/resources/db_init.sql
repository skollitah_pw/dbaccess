CREATE TABLE Aktorka (
    Id integer  primary key not null,
    Nazwisko varchar(100) not null,
    [Rok Urodzenia] smallint not null
);

CREATE TABLE Rezyser (
    Id integer  primary key not null,
    Nazwisko varchar(100) not null,
    [Rok Urodzenia] smallint not null
);

CREATE TABLE Film (
    Id integer  primary key not null,
    Tytul varchar(200) not null,
    [Rok Produkcji] smallint not null,
    [Rezyser Id] int not null,
    [Aktorka Id] int not null,
    FOREIGN KEY ([Rezyser Id]) REFERENCES Rezyser(Id),
    FOREIGN KEY ([Aktorka Id]) REFERENCES Aktorka(Id)
);