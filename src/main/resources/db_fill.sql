INSERT INTO Aktorka(Id, Nazwisko, [Rok Urodzenia]) VALUES (1, 'Michelle Pfeiffer', 1958);
INSERT INTO Aktorka(Id, Nazwisko, [Rok Urodzenia]) VALUES (2, 'Radha Louis-Mitchell', 1973);
INSERT INTO Aktorka(Id, Nazwisko, [Rok Urodzenia]) VALUES (3, 'Miranda Otto', 1967);

INSERT INTO Rezyser(Id, Nazwisko, [Rok Urodzenia]) VALUES (1, 'Richard Donner', 1930);
INSERT INTO Rezyser(Id, Nazwisko, [Rok Urodzenia]) VALUES (2, 'Peter Naess', 1960);
INSERT INTO Rezyser(Id, Nazwisko, [Rok Urodzenia]) VALUES (3, 'Robert Zemeckis', 1952);
INSERT INTO Rezyser(Id, Nazwisko, [Rok Urodzenia]) VALUES (4, 'Peter Jackson', 1961);

INSERT INTO Film(Id, Tytul, [Rok Produkcji], [Aktorka Id], [Rezyser Id])
    VALUES(1, 'Zakleta w Sokola', 1985, 1, 1);

INSERT INTO Film(Id, Tytul, [Rok Produkcji], [Aktorka Id], [Rezyser Id])
    VALUES(2, 'Zaklete serca', 2005, 2, 2);

INSERT INTO Film(Id, Tytul, [Rok Produkcji], [Aktorka Id], [Rezyser Id])
    VALUES(3, 'Co kryje prawda', 2000, 1, 3);

INSERT INTO Film(Id, Tytul, [Rok Produkcji], [Aktorka Id], [Rezyser Id])
    VALUES(4, 'Wladca pierscieni: Powrot krola', 2003, 3, 4);