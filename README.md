Program sklada sie z wielu klas oraz plikow konfiguracyjnych, dlatego prosta kompilacja poleceniem javac
jest niewygodna. Przygotowalem sie do cwiczenia i wiekszosc pracy wykonalem jeszcze w lipcu. Program korzysta z narzedzia Gradle i jest budowany jednym prostym
poleceniem. Nie potrzeba zadnych dodatkowych narzedzi poza Java 8.

Program realizuje funkcjonalnosc zadania 1 oraz 2 poniewaz jako argument mozna podac dowolny
plik zawierajacy polecenia SQL lub dowolna komende SQL, a wiec zarowno utworzyc baze jak
i wypelnic ja danymi.

Wszystkie poniższe polecenia należy uruchamiać w katalogu ze zrodlami

Budowa aplikacji Windows
```sh
gradlew build fatJar
```

Budowa aplikacji Linux
```sh
./gradlew build fatJar
```

Komenda tworzaca model bazy danych. Baza jest tworzona domysnie w tym samym katalogu, w ktorym uruchamiane sa polecania,
czyli w glownym katalogu ze zrodlami

```sh
java -jar build\libs\dbaccess-0.0.1-SNAPSHOT-all.jar exec -f "src\main\resources\db_init.sql" -c "src\main\resources\config.properties"
```
Komenda wypelniajaca baze danymi
```sh
java -jar build\libs\dbaccess-0.0.1-SNAPSHOT-all.jar exec -f "src\main\resources\db_fill.sql" -c "src\main\resources\config.properties"
```
Przykladowa komenda z instrukca SQL
```sh
java -jar build\libs\dbaccess-0.0.1-SNAPSHOT-all.jar exec -s "INSERT INTO Aktorka(Id, Nazwisko, [Rok Urodzenia]) VALUES (4, 'Angelina Jolie', 1975);" -c "src\main\resources\config.properties"
```